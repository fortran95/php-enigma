<?php

function selftestGPG(){
    if(!function_exists('exec')){
        return 'Function `exec` does not exist.';
    }

    $gpgOutput = array();
    exec('gpg --version', $gpgOutput);
    if(count($gpgOutput) < 1) return 'Seems GPG is not installed.';
    $gpgLine = $gpgOutput[0];

    return (substr($gpgLine, 0, 3) == 'gpg');
}
