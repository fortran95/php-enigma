<?php
define('TEMPLATE_DIR', '/../templates/');
require(dirname(__FILE__) . '/Smarty/Smarty.class.php');

class Output {

    private $outputErrors = array();
    
    private $outputWith = 'template';
    
    private $setCookies = array();
    
    private $templateName = null;
    private $templateVariables = array();
    
    private $redirectTo = null;

    private function generateErrorOutput(){
        $smarty = new Smarty();
        $smarty->setTemplateDir(dirname(__FILE__) . TEMPLATE_DIR);
        $smarty->assign('errors', $this->outputErrors);
        $smarty->display('error.tpl');
    }

    private function generateTemplateOutput(){
        $smarty = new Smarty();
        $smarty->setTemplateDir(dirname(__FILE__) . TEMPLATE_DIR);
        foreach($this->templateVariables as $varname=>$varvalue){
            $smarty->assign($varname, $varvalue);
        }
        $smarty->display($this->templateName . '.tpl');
    }


    public function cookie($varname, $value){
        $this->setCookies[$varname] = $value;
    }

    public function setOutputWithTemplate($tempname, $vars=array()){
        $this->outputWith = 'template';
        $this->templateName = $tempname;
        $this->templateVariables = $vars;
    }

    public function setOutputWithRedirect($to){
        $this->outputWith = 'redirect';
        $this->redirectTo = $to;
    }

    public function reportError($desc){
        $this->outputErrors[] = $desc;
    }

    public function __destruct(){
        /* Generates output when destructing. */

        /* If error set, output error and end output. */
        if(count($this->outputErrors) > 0){
            return $this->generateErrorOutput();
        }

        /* set cookies */
        foreach($this->setCookies as $cookieName=>$cookieValue){
            setcookie($cookieName, $cookieValue);
        }

        /* generate output */
        switch($this->outputWith){
            case 'template':
                $this->generateTemplateOutput();
                break;
            case 'redirect':
                // TODO
                break;
            default:
                break;
        }
    }

}
