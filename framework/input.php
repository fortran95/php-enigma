<?php

class Input {

    private $settings = null;

    public function __construct(){
        // TODO read system settings
    }

    public function setting($varname){
        if(array_key_exists($varname, $this->settings)){
            return $this->settings[$varname];
        }
        return null;
    }

    public function post($varname){
        if(array_key_exists($varname, $_POST)){
            return $_POST[$varname];
        }
        return null;
    }

    public function get($varname){
        if(array_key_exists($varname, $_GET)){
            return $_GET[$varname];
        }
        return null;
    }

    public function cookie($varname){
        if(array_key_exists($varname, $_COOKIE)){
            return $_COOKIE[$varname];
        }
        return null;
    }

}
