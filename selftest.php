<?php

require("_framework.php");

require("selftests/gpg.php");
if(true !== $report = selftestGPG()){
    $OUTPUT->reportError('GPG Test:' . $report);
    return;
}

$OUTPUT->setOutputWithTemplate('selftest');
