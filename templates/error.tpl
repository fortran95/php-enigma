{include 'header.tpl' title='Enigma - 错误'}
<div class="container">

    <div class="alert alert-danger">
        程序执行中出现错误。
    </div>

    {section name=id loop=$errors}
    <div>{$errors[id]}</div>
    {/section}

</div>
{include 'footer.tpl'}
