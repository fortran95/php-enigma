{include 'header.tpl' title='Enigma'}

{include 'navbar.tpl'}

<div class="container">
{block "body"}{/block}
</div>

{include 'footer.tpl'}
