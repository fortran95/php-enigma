<html>
<head>
<title>{$title}</title>
<link rel="stylesheet" href="./static/css/bootstrap.min.css"/>
<link rel="stylesheet" href="./static/css/bootstrap-theme.min.css"/>
<link rel="stylesheet" href="./static/css/main.css"/>
<script src="./static/js/bootstrap.min.js"></script>
<script src="./static/js/jquery.min.js"></script>
<script src="./static/js/main.js"></script>
</head>
<body>
<div class="javascript-disabled alert alert-danger">
<strong>错误！</strong>
您必须启用浏览器的Javascript才能使用本系统。
</div>
<div class="javascript-enabled">
